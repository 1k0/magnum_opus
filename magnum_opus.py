import os
import sys
from collections import namedtuple
import multiprocessing
import subprocess
import time
import uuid
import shutil
import tempfile
from PIL import Image

if os.name == 'nt' : 
	import win32api
	import win32con

PARAM_EXIT_MESSAGE = "Please run the program again with correct parameters. The program will terminate now."
PARAM_PARSE_ERROR_MESSAGE = "ERROR invalid %s parameter. The program will terminate now."
PARAM_PARSE_WARN_MESSAGE = "WARNING invalid %s parameter using default."
PARAM_NO_FOLDER_ERROR_MESSAGE = "ERROR no start folder specified. The folder parameter is mandatory."

FileActions = namedtuple("FileActions", "keep remove")
CoverActions = namedtuple("CoverActions", "keep remove fromImage")
ParamHandler = namedtuple("SwitchHandler", "function setter range isOptional")

class Params : 
	ENCODING_MODES = ["vbr", "hard-cbr", "cvbr"]
	FILE_ACTIONS = FileActions("keep", "remove")
	COVER_ACTIONS = CoverActions("keep", "remove", "fromImage")
	MAX_THREAD_COUNT = max(1, (multiprocessing.cpu_count() - 1))
	MIN_BITRATE = 32
	MAX_BITRATE = 512
	DEFAULT_BITRATE = 256

	def __init__(self) : 
		self.srcFolder = None
		self.dstFolder = None
		self.threadCount = Params.MAX_THREAD_COUNT
		self.encodingBitrate = Params.DEFAULT_BITRATE
		self.encodingMode = Params.ENCODING_MODES[0]
		self.fileAction = Params.FILE_ACTIONS[0]
		self.coverAction = Params.COVER_ACTIONS[0]

	def srcFolder(self, value) : 
		self.srcFolder = value

	def dstFolder(self, value) : 
		self.dstFolder = value

	def threadCount(self, value) : 
		self.threadCount = value

	def encodingBitrate(self, value) : 
		self.encodingBitrate = value

	def encodingMode(self, value) : 
		self.encodingMode = value

	def fileAction(self, value) : 
		self.fileAction = value

	def coverAction(self, value)  :
		self.coverAction = value

	def print(self) : 
		print("------------------------------Parameters---------------------------------------")
		print("Source folder : ", self.srcFolder)
		print("Destination folder : ", self.dstFolder)
		print("Thread count : ", self.threadCount)
		print("Encoding bitrate : ", self.encodingBitrate)
		print("Encoding mode : ", self.encodingMode)
		print("File action : ", self.fileAction)
		print("Coverart action : ", self.coverAction)
		print("-------------------------------------------------------------------------------")

	def __printUsage(self) : 
		print("----------------------------------Usage----------------------------------------")
		print('srcFolder="<valid path to source folder>"')
		print('mandatory | has to be a valid path')
		print('')
		print('dstFolder="<valid path to destination folder>"')
		print('optional | has to be a valid path')
		print('')
		print('threadCount=<number>')
		print('optional | range : [1..%d] | default : %d' % (Params.MAX_THREAD_COUNT, self.threadCount))
		print('')
		print('encodingBitrate=<number>')
		print('optional | range : [%d..%d] | default : %d' % (Params.MIN_BITRATE, Params.MAX_BITRATE, self.encodingBitrate))
		print('')
		print('encodingMode=<%s|%s|%s>' % (Params.ENCODING_MODES[0], Params.ENCODING_MODES[1], Params.ENCODING_MODES[2]))
		print('optional | default : %s' % self.encodingMode)
		print('vbr : In VBR mode the bitrate may go up and down freely depending on the content to achieve more consistent quality.')
		print('hard-cbr : Use hard constant bitrate encoding. With hard-cbr every frame will be exactly the same size.')
		print('cvbr : Use constrained variable bitrate encoding. Outputs a specific bitrate. This mode is analogous to CBR in AAC and '
		'MP3 encoders and managed mode in Vorbis coders. This delivers less consistent quality than VBR mode but consistent bitrate.')
		print('')
		print('fileAction=<%s|%s>' % (Params.FILE_ACTIONS[0], Params.FILE_ACTIONS[1]))
		print('optional | default : %s' % self.fileAction)
		print('')
		print('coverAction=<%s|%s|%s>' % (Params.COVER_ACTIONS[0], Params.COVER_ACTIONS[1], Params.COVER_ACTIONS[2]))
		print('optional | default : %s' % self.coverAction)
		print("-------------------------------------------------------------------------------")

	def __parseAsPath(self, path:str, setter, dummy) : 
		if (os.path.isdir(path)) : 
			setter(self, path)
			return True
		return False

	def __parseAsInt(self, number:str, setter, range) : 
		if (number.isdigit()) : 
			n = int(number)
			if (n in range) : 
				setter(self, n)
				return True
		return False
	
	def __parseAsEnum(self, value:str, setter, enum) :
		if (value in enum) : 
			setter(self, value)
			return True
		return False

	def parse(self, argList : []) : 
		if (len(argList) == 1) : 
			self.__printUsage()
			sys.exit(PARAM_EXIT_MESSAGE)

		PARAM_HANDLERS = {
			"srcFolder" : ParamHandler(Params.__parseAsPath, Params.srcFolder, None, False),
			"dstFolder" : ParamHandler(Params.__parseAsPath, Params.dstFolder, None, True),
			"encodingBitrate" : ParamHandler(Params.__parseAsInt, Params.encodingBitrate, range(Params.MIN_BITRATE, Params.MAX_BITRATE), True),
			"threadCount" : ParamHandler(Params.__parseAsInt, Params.threadCount, range(1, Params.MAX_THREAD_COUNT), True),
			"encodingMode" : ParamHandler(Params.__parseAsEnum, Params.encodingMode, Params.ENCODING_MODES, True),
			"fileAction" : ParamHandler(Params.__parseAsEnum, Params.fileAction, Params.FILE_ACTIONS, True),
			"coverAction" : ParamHandler(Params.__parseAsEnum, Params.coverAction, Params.COVER_ACTIONS, True),
		}

		for i in range(1, len(argList)) : 
			paramString = argList[i]
			paramKVP = paramString.split("=")
			param = paramKVP[0]

			if (param not in PARAM_HANDLERS) : 
				print("WARNING unknown parameter : ", param)
				continue

			if (len(paramKVP) != 2) : 
				print(PARAM_PARSE_WARN_MESSAGE % param)
				continue

			value = paramKVP[1]
			handler = PARAM_HANDLERS[param]
			valueParsed = handler.function(self, value, handler.setter, handler.range)
			if (valueParsed == False) : 
				if (handler.isOptional) : 
					print(PARAM_PARSE_WARN_MESSAGE % param)
				else : 
					sys.exit(PARAM_PARSE_ERROR_MESSAGE % param)

		if (self.srcFolder == None) : 
			sys.exit(PARAM_NO_FOLDER_ERROR_MESSAGE)

class ManagedFile : 
	def __init__(self, path:str) : 
		self.path = path
		self.keep = False

	def keep(self) : 
		self.keep = True

	def __del__(self) :
		if (self.path and self.keep == False) : 
			print("[ManagedFile] File deleted : ", self.path)
			try : 
				os.remove(self.path)
			except OSError :
				pass

class CoverartFactory : 
	def __init__(self, params:Params) :
		self.tempFolder = tempfile.mkdtemp()
		self.enabled = (params.coverAction == Params.COVER_ACTIONS.fromImage)
		print("[CoverartFactory] Temporary folder created : ", self.tempFolder)

	def __del__(self) : 
		shutil.rmtree(self.tempFolder)
		print("[CoverartFactory] Temporary folder deleted : ", self.tempFolder)

	def __makeTempJpgPath(self) :
		return os.path.join(self.tempFolder, (uuid.uuid1().hex + ".jpg"))

	def __createForImage(self, sourceImagePath:str) :
		coverartPath = self.__makeTempJpgPath()
		image = Image.open(sourceImagePath)
		image = image.resize((512, 512))
		image.save(coverartPath)

		return coverartPath

	def createForFolder(self, currentFolder:str, files:[str]) : 
		if (self.enabled) : 
			for file in files : 
				lower = file.lower()
				if (lower in {"cover.jpg", "cover.jpeg", "front.jpg", "front.jpeg"}) : 
					path = os.path.join(currentFolder, file)
					print("[CoverartFactory] Coverart created for : ", path)
					return self.__createForImage(path)

		return None

class OpusTask : 
	def __init__(self, process:subprocess.Popen, sourceFile:ManagedFile, coverart:ManagedFile) : 
		self.process = process
		self.sourceFile = sourceFile
		self.coverart = coverart

	def __del__(self) : 
		if ((self.sourceFile is not None) and (self.process.returncode != 0)) :
			self.sourceFile.keep()

class OpusEncoder : 
	def __init__(self, params:Params) : 
		self.params = params
		self.slots = [None] * params.threadCount
		self.LAUNCH_STRING = "opusenc --quiet --bitrate %d --%s" % (params.encodingBitrate, params.encodingMode)

	def __isSlotDone(self, i:int) : 
		return ((self.slots[i] is None) or (self.slots[i] and self.slots[i].process.poll() is not None))

	def __waitForFreeSlot(self) : 
		freeSlot = -1
		while (freeSlot < 0) : 
			for i in range(len(self.slots)) :
				if (self.__isSlotDone(i)) : 
					self.slots[i] = None
					freeSlot = i
					break
			time.sleep(0.1)
		return freeSlot

	def encode(self, sourceFile:str, destFile:str, coverart:ManagedFile) : 
		print("[OpusEncoder] encoding : ", sourceFile)
		launchString = self.LAUNCH_STRING
		if (self.params.coverAction != Params.COVER_ACTIONS.keep) : 
			launchString += " --discard-pictures"
		if coverart.path : 
			launchString += ' --picture "%s"' % (coverart.path)
		launchString += ' "%s" "%s"' % (sourceFile, destFile)
		sourceFile = ManagedFile(sourceFile) if (self.params.fileAction == Params.FILE_ACTIONS.remove) else None
		freeSlot = self.__waitForFreeSlot()

		self.slots[freeSlot] = OpusTask(subprocess.Popen(launchString, creationflags=subprocess.CREATE_NO_WINDOW), sourceFile, coverart)

	def waitForAllToFinish(self) : 
		for i in range(len(self.slots)) : 
			if (self.slots[i] is not None) : 
				self.slots[i].process.wait()
				self.slots[i] = None

def IsHidden(path) : 
	if os.name == 'nt' : 
		attribute = win32api.GetFileAttributes(path)
		return attribute & (win32con.FILE_ATTRIBUTE_HIDDEN | win32con.FILE_ATTRIBUTE_SYSTEM)
	else : 
		name = os.path.basename(path)
		return name.startswith('.')

def IsFlac(path) :
	ext = os.path.splitext(path)[1]
	ext = ext.lower()
	return (ext == ".flac")

def ReplaceExtension(path, newExtension) : 
	return (os.path.splitext(path)[0] + newExtension)

def UserConfirm(yesNoQuestion:str) : 
	isUserRetarded = True
	userInput = None
	while (isUserRetarded) : 
		userInput = input("%s (yes/no) : " % yesNoQuestion)
		userInput = userInput.lower()
		isUserRetarded = (userInput not in ["yes", "no"])

	return (userInput == "yes")

def MakeDestFolder(params:Params, currentFolder:str) :
	if (params.dstFolder) : 
		destFolder = params.dstFolder
		if (currentFolder != params.srcFolder) : 
			relativePath = os.path.relpath(currentFolder, params.srcFolder)
			destFolder = os.path.join(destFolder, relativePath)
		#Create destination folder
		if (params.dstFolder) : 
			os.makedirs(destFolder, exist_ok = True)
		return destFolder

	return currentFolder

def main() : 
	print("-----------------------------MAGNUM-OPUS---------------------------------------")
	params = Params()
	params.parse(sys.argv)
	params.print()

	if (UserConfirm("Are these parameters ok?") == False) : 
		sys.exit("Please run the program again with correct parameters. The program will exit now.")
	else : 
		print("Proceeding with conversion...")

	coverartFactory = CoverartFactory(params)
	opusEncoder = OpusEncoder(params)

	for currentFolder, subFolders, files in os.walk(params.srcFolder) :
		if (IsHidden(currentFolder) == False) : 
			print("[Walker] Processing folder : ", currentFolder)

			destFolder = MakeDestFolder(params, currentFolder)
			coverart = ManagedFile(coverartFactory.createForFolder(currentFolder, files))

			for file in files :
				filePath = os.path.join(currentFolder, file)
				if ((IsHidden(filePath) == False) and IsFlac(filePath)) : 
					flacPath = filePath
					opusPath = os.path.join(destFolder, ReplaceExtension(file, ".opus"))
					opusEncoder.encode(flacPath, opusPath, coverart)

	opusEncoder.waitForAllToFinish()

main()